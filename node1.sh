sudo yum install -y yum-utils device-mapper-persistent-data lvm2

sudo yum-config-manager     --add-repo     https://download.docker.com/linux/centos/docker-ce.repo

yum install -y docker-ce-19.03.1

systemctl enable docker
systemctl start docker

cat <<-EOF > /etc/yum.repos.d/kubernetes.repo
	[kubernetes]
	name=Kubernetes
	baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
	enabled=1
	gpgcheck=1
	repo_gpgcheck=1
	gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF

sudo yum install -y kubelet-1.19.14 kubeadm-1.19.14 kubectl-1.19.14

systemctl enable kubelet
systemctl start kubelet

sudo hostnamectl set-hostname node1

cat <<-EOF > /etc/sysctl.d/k8s.conf
	net.bridge.bridge-nf-call-ip6tables = 1
	net.bridge.bridge-nf-call-iptables = 1
	fs.file-max = 1500000
	fs.nr_open = 1500000
	vm.max_map_count = 262144
	net.ipv4.ip_local_reserved_ports = 30000-32767
	net.ipv4.ip_local_port_range = 1024 65535
	net.netfilter.nf_conntrack_max = 32768000
	vm.swappiness = 0
	net.ipv4.ip_forward = 1
EOF

sysctl --system 1> /dev/null

sudo setenforce 0

sudo sed -i 's/^SELINUX=enforcing$/SELINUX=permissive/' /etc/selinux/config

sudo sed -i '/swap/d' /etc/fstab

sudo swapoff -a

kubeadm config images pull